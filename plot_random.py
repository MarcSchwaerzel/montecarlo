# -*- coding: utf-8 -*-
"""
Created on Mon Apr  8 15:41:01 2019

@author: Marc.Schwaerzel
"""
import numpy as np
import matplotlib.pyplot as plt
from pylab import *

def plot_mu(x, y, name_ax1='ax1', name_ax2='ax2'):
    fig = plt.figure()
    plt.plot(x,y)    
    plt.xlabel(name_ax1)
    plt.ylabel(name_ax2)
    plt.show()

def plot_hist(x, bins=20, name_ax1='ax1', name_ax2='ax2'):
    fig = plt.figure()
    plt.hist(x,bins=bins)
    plt.xlabel(name_ax1)
    plt.ylabel(name_ax2)
    plt.show()     
    
def plot_polar(theta, phi, r, sza, wl):
#    fig = plt.figure()
#    plt.polar(angles, r)
#    plt.show()       
    fig = figure(figsize=(8,8)) 
     
    # Create axes for polar plot
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.8], polar=True)
     
    # number of contours
    N=50
    contourf([phi*pi/180, 180.-(theta)*180/pi], N, cmap=cm.jet)
    # see help(colormap) for available color schemes 
    colorbar(shrink=0.7)
    # specify limits for colors if needed
    # clim(20,100)
     
#    text(0.55, 1.1, 'Polar plot of radiance field for nstr=%02d' %nstr, fontsize=18,
#         horizontalalignment='center',
#         verticalalignment='center',
#         transform = ax.transAxes)
    fig.show()
    # Save file 
    savefig('/home/r/Raphael.Kriegmair/uni/master/monteCarlo/montecarlo/figures/irradiance_sza:%i_wl:%i_pho0:90.png' %(sza, wl))