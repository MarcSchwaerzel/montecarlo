# -*- coding: utf-8 -*-
"""
Created on Mon Apr  8 15:41:01 2019

@author: Marc.Schwaerzel
"""
import numpy as np
import config

def rand_mu_rayleigh():
    x = np.random.rand()
    q = -8*x + 4
    D = 1 + 0.25*np.power(q,2)
    u = np.power(-q/2 + np.sqrt(D), 1./3)
    mu = u - 1/u
    return mu
    
def rand_mu_HG(g=config.default.g):
    x = np.random.rand()   
    mu = 1/(2*g) * (-((g**2 - 1 )/(2*g*x - g - 1))**2 + g**2 + 1)
    return mu    

def rand_path(beta=1):
    x = np.random.rand()   
    s = -np.log(1-x)/beta
    return s
    
def rand_sfc_refl():
    nums = np.random.rand()
    mu = np.sqrt(nums)
    phi = np.random.rand() * 2 * np.pi
    return mu, phi 
    
def P_HG_funct(x,g=0.85):
    return (1 - g**2)/np.power(1 + g**2 - 2*g*x, 3./2)
    

def rand_abs_photon(b_abs, b_sca):
    w0 = b_sca/(b_abs + b_sca)
    if np.random.rand() >= w0:
        absoption = True
    else:
        absoption = False
    return absoption

def cond_sfc_refl(albedo=0.2):
    nums = np.random.rand()
    if nums < albedo:
        reflection = True
    else:
        reflection = False
    return reflection

def cloud(b_sca, b_cld):
    nums = np.random.rand()
    B = b_sca /(b_sca + b_cld)
    if nums > B:
        cloud = True
    else:
        cloud = False
    return cloud
