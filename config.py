# -*- coding: utf-8 -*-
"""
Created on Tue Apr  9 10:53:35 2019

CONFIG

@author: Raphael.Kriegmair
"""

# single layer MC model
import numpy as np

class cloud1:
    zmax = 100.
    zmin = 0.
    beta = .1
    
class photon:
    z = 10.
    phi0 = 0
    sza = 0
    
class default:
    g = 0.85

def import_atm_file(wl=300):
    '''
    import the given atm file with colomns:
    z [km]    k_sca [km-1]   k_abs [km-1]     
    '''
    data = np.loadtxt('./data/ksca_kabs_lambda%i.dat' % wl)
    z = data[:,0]
    k_sca = data[:,1]
    k_abs = data[:,2]
    return z, k_sca, k_abs 
    