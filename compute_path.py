# -*- coding: utf-8 -*-
"""
Created on Tue Apr  9 16:42:54 2019

@author: Raphael.Kriegmair
"""
import config
import compute_random as MC_rand
import numpy as np

def init_photon(theta0=config.photon.sza, phi0=config.photon.phi0):
    theta0 = np.deg2rad(theta0)
    phi0 = np.deg2rad(phi0)
    n = np.array([np.sin(theta0)*np.cos(phi0), np.sin(theta0)*np.sin(phi0), -np.cos(theta0)])
    return n
    
def propagate_sing(z1, n, tau, beta=config.cloud1.beta):
    z2 = z1 + n[2] * tau / beta
    return z2
    
def scatter(n_old, scatter_type='cld', g=0.85): 
    '''
    this function computes the scattering of a photon for clouds 
    (scattertype=cld) or for molecular scattering (reyleigh) (scattertype=mol)
    '''
    phi = np.random.rand()*2*np.pi
    if scatter_type == 'mol':
        theta = np.arccos(MC_rand.rand_mu_rayleigh())
    elif scatter_type == 'cld':
        theta = np.arccos(MC_rand.rand_mu_HG(g))
    else:
        raise ValueError("not a defined scattering type, please use mol or cld")
    n_old = normalize(n_old)
    
    if n_old[1] != 0 and n_old[2] != 0:
        w = np.array([1,0,0])
    else:
        w = np.array([0,1,0])
    u = np.cross(n_old, w)
    u = normalize(u)
    v = np.cross(n_old, u)
    v = normalize(v)
    n_new = np.cos(theta)*n_old + np.sin(theta)*normalize((np.cos(phi)*u + np.sin(phi)*v))
    return n_new

def normalize(v):
    norm = np.sqrt(v[0]**2 + v[1]**2 + v[2]**2)
    return v/norm
    
def determine_z_index(z_atm, z):
    diff = z_atm - z                     
    idx = (np.abs(diff)).argmin()
    if diff[idx] >= 0:
        idx = idx - 1
    return idx
    
def tau_layer(z, mu, beta):
    return z/mu * beta