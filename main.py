# -*- coding: utf-8 -*-
"""
Created on Mon Apr  8 15:41:01 2019

@author: Marc.Schwaerzel
"""
import numpy as np
import compute_random as MC_rand
import plot_random as MC_plot
import config
import compute_path as path
#from multiprocessing import Pool

# daty 3 multiple layer

def mult_layer(sza, wl):
    '''
    this functions simulates photon for a give wavelength and solar zenith 
    angle and counts the photons arriving at the boundaries (transmittance and
    radiance)    
    '''
    # input file atmospheric beta_sca and b_abs  
    z_atm, b_sca, b_abs = config.import_atm_file(wl=wl)
    b_sca = b_sca[::-1] 
    b_abs = b_abs[::-1]
    z_atm = z_atm[::-1]
#    z_diff = np.diff(z_atm) 
    z_atm = z_atm[:-1]
    
    # input parameter
    num_photons = 50000
    b_CLD = 0.9
    cloud_ind = 400
    albedo = 0.0
    phi0 = 90
    
    # initializing counters
    counter_top = 0
    counter_bottom = 0
    counter_abs = 0
    counter_irr = 0
    angle_irr = 5
    theta_plot = []
    radius = []
    phi_plot = []
    
    for j in range(num_photons): 
        # initialize photon     
        alive = True
        n = path.init_photon(theta0=sza, phi0=phi0) 
        z = z_atm.max() + 5
        idx = path.determine_z_index(z_atm=z_atm, z=z) # index of bottom layer of current cell
 
        while alive: ## photon interraction file
            mu = n[2]
            tau = -np.log(1-np.random.rand())

            while tau > 0: ## if tau is still < 0
                ## find closest border in direction of movement
                if mu < 0: ## photon going downwards
                    z_next = z_atm[idx] # below photon
                    if z == z_next and z != 0:
                        z_next = z_atm[idx-1]
                elif mu > 0: ## photon going upwards
                    if idx == len(z_atm[:])-1:
                        z_next = z_atm[idx] + 5 # above photon
                    else:
                        z_next = z_atm[idx+1] # above photon
                else: ## photon travelling horizontally
                    tau = 0
                    z_next  = z + 1 #
                    
                ## read out the betas
                b_SCA = b_sca[idx]
                b_ABS = b_abs[idx]
                b_EXT =  b_SCA +  b_ABS
                cloud = False
                
                ## test if we are in the cloud layer
                if idx == cloud_ind:
                    cloud = MC_rand.cloud(b_sca=b_SCA, b_cld=b_CLD)
                if cloud:
                    b_EXT = b_SCA +  b_ABS + b_CLD
                    
                ## compute optical pathlength of remaining distance
                distance = z - z_next
                tau_layer_remain = abs(path.tau_layer(distance, mu=mu, beta=b_EXT))

                ## photon stays inside current cell
                if tau - tau_layer_remain < 0:
                    z = path.propagate_sing(z, n, tau, beta=b_EXT)
                    tau = 0
                    
                    ## ABSORPTION
                    if MC_rand.rand_abs_photon(b_abs=b_ABS, b_sca=b_SCA):
                        #print('absorbed')
                        counter_abs += 1 
                        alive = False
                        
                    ## SCATTERING
                    else: 
                        if tau != 0:
                            raise ValueError('scatter before tau 0')
                        if cloud:
                            n = path.scatter(n, scatter_type='cld', g=0.85)
                        else:
                            n = path.scatter(n, scatter_type='mol')

                ## photon moves into next cell
                else:
                    tau = tau - abs(tau_layer_remain)  
                    
                    ## test photon arrives at the ground
                    if idx == 0 and mu < 0:
                        cond_refl = MC_rand.cond_sfc_refl(albedo=albedo)
                        if -np.arccos(mu) <= np.deg2rad(angle_irr):
                            counter_irr += 1
                        theta_plot.append(-np.arccos(mu))
                        phi_plot.append(np.arccos(n[0]/np.sin(np.arccos(mu))))
                        radius.append(1)
                        if cond_refl:
                            mu, phi = MC_rand.rand_sfc_refl()
                            z = 0
#                            print('surface reflection')
                        else:
                            counter_bottom += 1
                            #print('out_bottom')
                            tau = 0
                            alive = False
                    ## test photon arrives at TOA
                    elif idx == len(z_atm[:])-1 and mu > 0:
                        counter_top += 1
                        #print('out_top')
                        alive = False
                        tau = 0
                    else:    
                        if mu > 0:
                            idx += 1
#                            print('pass_up')
#                            print('index: ', idx)
                        elif mu < 0:
                            idx -= 1
#                            print('pass_down')
#                            print('index: ', idx)    
                        z = z_atm[idx]
   
    MC_plot.plot_polar(np.array(theta_plot), np.array(phi_plot),
                       np.array(radius), sza, wl)
    print(str(counter_bottom) + ' (bottom) + ' + str(counter_top) + ' (top) + ' + str(counter_abs) + ' (abs) = ' + str(counter_top + counter_bottom + counter_abs) + ' (' + str(num_photons) + ')')
    print(str(float(counter_bottom)/num_photons) + ' T : R ' + str(float(counter_top)/num_photons))
            
    print('sigma_top: ' + str(np.sqrt(float(num_photons-counter_top)/(num_photons*counter_top))))
    print('sigma_bottom: ' + str(np.sqrt(float(num_photons-counter_bottom)/(num_photons*counter_bottom))))
    print('irradiance', counter_irr)

if __name__ == "__main__":
    sza=[0,0,0,0,30,30,30,30,60,60,60,60]
    wl_i =[320,350,400,500,320,350,400,500,320,350,400,500]
    for ix, i in enumerate(sza):
            sza = i
            wl = wl_i[ix]
            print(' ')
            print('wl: ', wl, 'sza ', sza)
            mult_layer(sza, wl)


def task1():
    mu = MC_rand.rand_mu_rayleigh()
    MC_plot.plot_hist(mu, bins=40, name_ax1='mu',name_ax2='# r(mu)')   
    
def task2():
    mu = MC_rand.rand_mu_HG()
#    x = np.linspace(-1,1,100)
    MC_plot.plot_hist(mu, bins=40, name_ax1='mu',name_ax2='# r(mu)') 

def task3():  
    s = MC_rand.rand_path()
    MC_plot.plot_hist(s, bins=40, name_ax1='photon pathlength s',
                      name_ax2='# r(s)') 
                      
def task4(): 
    rad = MC_rand.rand_sfc_refl()
    MC_plot.plot_hist(rad, bins=40, name_ax1='mu',
                      name_ax2='# r(s)')
          
# single layer            
  
#def task():
def day2_singlelayer():
    beta = [.01,.1,1, .01,.1,1, .01,.1,1]
    sza = [0, 0, 0, 30, 30, 30, 60, 60, 60]
    
    for ix, i in enumerate(beta):
        beta = i
        sza_in = sza[ix]
          
        num_photons = 5000
        counter_top = 0
        counter_bottom = 0
        
        for j in range(num_photons):
        #    print j
        
            z, n = path.init_photon(theta0=sza_in)  
            while 0 <= z and z <= config.cloud1.zmax: 
        #        print n
                z = path.propagate_photon(z, n, beta=beta)
                n = path.scatter(n)
                  
            if z > config.cloud1.zmax:
                counter_top += 1
            else:
                counter_bottom += 1
        
        print('tauc: '+ str(beta*config.cloud1.zmax) + ', sza: ' + str(sza_in))    
        print(str(counter_bottom) + ' + ' + str(counter_top) + ' = ' + str(counter_top + counter_bottom) + ' (' + str(num_photons) + ')')
        print(str(float(counter_bottom)/num_photons) + ' T : R ' + str(float(counter_top)/num_photons))
            
        print('sigma_top: ' + str(np.sqrt(float(num_photons-counter_top)/(num_photons*counter_top))))
        print('sigma_bottom: ' + str(np.sqrt(float(num_photons-counter_bottom)/(num_photons*counter_bottom))))
    


    
